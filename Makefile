# Author: M. Fauzilkamil Bin Zainuddin <m.fauzilkamil@songketmail.org>
curdir := $(shell pwd)

up:
	podman run --rm -it -v $(curdir):/app -w /app --net host -p 8888:8888/tcp  python bash


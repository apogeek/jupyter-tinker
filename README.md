# Tinkering with Python

This is a random repo for my personal tinkering with python in jupyter notebook.

## Setup

Create venv
```
python3 -m venv venv
. venv/bin/activate
```

## Start Jupyter
```
make up
. venv/bin/activate
jupyter notebook --allow-root

```

